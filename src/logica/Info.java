/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;


/**
 *
 * @author jorge.cardenas
 */
public class Info {
    private byte hex;
    private String Ascii;

    public byte getHex() {
        return hex;
    }

    public void setHex(byte hex) {
        this.hex = hex;
    }

    public String getAscii() {
        return Ascii;
    }

    public void setAscii(String Ascii) {
        this.Ascii = Ascii;
    }
    
    
}
