package presentacion;

import java.io.File;
import java.util.Arrays;
import javax.swing.JFileChooser;
import static logica.File.convertHexToString;
import static logica.File.getByteArrayFileContent;
import logica.Info;

public class Modelo implements Runnable {

    private VistaPrincipal ventanaPrincipal;
    private Thread hiloDibujo;
    private Info miSistema;
    private JFileChooser ofile;

    public void iniciar() {
        getVentanaPrincipal().setSize(700, 400);
        getVentanaPrincipal().setVisible(true);
        hiloDibujo = new Thread(this);
        hiloDibujo.start();
        ofile = new JFileChooser();
        ofile.setCurrentDirectory(new File("./temp"));   
    }

    public Info getMiSistema() {
        if (miSistema == null) {
            miSistema = new Info();
        }
        return miSistema;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }
    public void archivo(){  
        int resultfile = ofile.showOpenDialog(ofile);
        if (resultfile == JFileChooser.APPROVE_OPTION) {
            try {
                byte[] file = getByteArrayFileContent("prueba.txt", ofile.getSelectedFile());
                System.out.println("file: " + Arrays.toString(file));
                for (int i = 0; i < file.length; i++) {
                    byte b = file[i];
                    String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                    System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
                    getMiSistema().setHex(b);
                    getMiSistema().setAscii(convertHexToString);
                }
            } catch (Exception e) {
            }
        } else {
            System.out.println("No se cargo el archivo");
        }
    }

    @Override
    public void run() {
        
    }
}